#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import requests
import logging
import sqlite3
import sys
import re
import os
from time import sleep
from bs4 import BeautifulSoup

dirname = os.path.dirname(os.path.realpath(__file__))

conn = sqlite3.connect(dirname + '/movies.db')
cur = conn.cursor()

logging.basicConfig(level=logging.INFO)

pat = re.compile(u'^\[\s*([^\]]*雷)\\s*]\s*(.*)')

host = 'www.ptt.cc'
page = "/bbs/movie/index.html"

while True:

    url = "https://{}{}".format(host, page)
    logging.info("Parsing {}".format(url)) 

    r = requests.get(url)
    
    if r.status_code != 200:
        logging.info("fail")
    
    
    bs = BeautifulSoup(r.text, "html.parser")
    
    for ent in bs.find_all("div", class_ = "r-ent"):
        detail = ent.find("a")
        if not detail:
            continue
        href = detail.attrs['href']
        date = ent.find("div", class_ = "date").contents[0].strip()
        title = detail.contents[0]
        m = pat.match(title)
        if m:
            cur.execute("select * from movies where link = ?", (href, ))
            if not cur.fetchone():
                conn.execute("insert into movies values (?, ?, ?, ?)", (href, m.group(1), m.group(2), date))
            else:
                logging.info("entry has been saved")
                sys.exit()
    
    
    conn.commit()
    
    for ent in bs.find(id = "action-bar-container").find_all("a"):
        if ent.contents[0].endswith(u"上頁"):
            page = ent.attrs['href']

    sleep(2)
